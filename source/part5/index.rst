.. _part5:

*************************************************************************************************
Partie 5 | Garbage Collection
*************************************************************************************************

Cheney algorithm proposed by Group 19, Arthur Sluyters and Benjamin Simon
==========================================================================

The Cheney algorithm uses the Forward function. 
Give its pseudo code and explain the cases that it has to handle.

Answer
"""""""
.. code-block:: java

	function Forward(p) {
		if (p points to from-space) {
			if (p.f1 points to to-space) { 
				return p.f1 //already moved: we just need to change the pointer
				
			} else {
				//we have to moved the object from from-space to to-space
				
				for (field fi : p) {
					next.fi = p.fi
				}
				p.f1 = next
				next += size of record p
				return p.f1
			}
		} else {
			return p //the job has already been done: nothing to do
			
		}
	}

Question 2
"""""""""""

Apply the algorithm to the given heap. Explain briefly each step.
	.. image:: img/cheney1.svg
		:alt: Question 2
		:height: 400

Answer
"""""""
	.. image:: img/cheney2.svg
		:alt: Answer2 Part 1
		
	.. image:: img/cheney3.svg
		:alt: Answer2 Part 2
		
		
		
Mark & Sweep Phase proposed by Group 35, Jérémy Kraus and Elias Oumouadene
==========================================================================

1. Clearly explain what are the purpose of the mark and sweep phases
2. Give the pseudo-code of the Sweep phase
3. Explain what is the amortized cost of collection and the complexity of the Sweep phase where H is the heap size

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer :

1. The Mark phase starts from the roots, traces the object in the graph and marks every object reached. 
( A root is an object referenced from anywhere in the call stack )
The Sweep phase iterates through all objects in the heap and reclaims unmarked objects and clear the marks.

2. The pseudo code of the Sweep phase is 
.. code-block:: jave

  p <- first address in heap

  while p < last address in heap

    if record p is marked 
        unmark p
    else let f1 be the first field in p
        p.f1 <- free list

        freelist <- p

    p <- p+(size of record p)

3.The amortized cost of collecting is the time to collect divided by the amount of garbage reclaimed. (c1*R+c2*H)/(H-R)
Sweep = O(H)

Reference counting and Mark phase proposed by Group 2, Maxime Mawait and Luis Tascon Gutierez
=============================================================================================

Question 1
""""""""""

1. Explain how Reference Counting works
2. Give its advantages.
3. Give its disadvantages.
4. Is Reference Counting used in practice in a language? (If yes, give one name of language)

Answer
""""""

1. Reference Counting:

	- give each object a special field that is the number of references to it.
	- whenever a new reference is made, increment field by 1.
	- whenever a reference is removed, decrement field by 1.
	- when reference count goes to zero, collect chunk.

2. Reference Counting is:

	- relatively fast (it does collecWon immediately on garbage, as soon as a
          counter becomes 0)
	- safe (never throws away non-garbage)

3. Reference Counting has problems with cycles.
4. Yes, it is used by Swift and Objective-C

Question 2
""""""""""

1. Explain how the mark phase of the mark & sweep works.
2. Give the algorithm used by the mark phase and its standard pseudo-code.
3. Explain the problem if you use this algorithm.
4. Give and explain the technique to use in order to fix it.

Answer
""""""

1. The Mark phase starts from the roots, traces the object in the graph and marks every object reached. 
( A root is an object referenced from anywhere in the call stack )
2. It is a DFS. It must be called on root pointer v.

.. code-block:: python

	function DFS(x):
		if x is a pointer into the heap:
			if record x is not marked:
				mark x
				for each field fi of record x:
					DFS(x.fi)

3. The main problem is the cost of the algorithm:

	- The time is proportionnal of the number of nodes it marks.
	- DFS is recursive (max depth = up to longest path of reachable data)
	- If H is the heap size, can take O(H) size (not acceptable)

We can not afford to have an algorithm which runs through the entire heap.
4. We can use the pointer reversal. Its main idea is to do a depth first traversal in the pointer graph itself, by reversing pointers as we go.

"""""""""""""""""""""""""""""""""""""""""""""""""""""""

Garbage collection : 2 phases proposed by Group 41, Benjamin Rémiche and Sanae Abdelouassaa
=============================================================================================

1. What's the 2 phases/steps of  garbage collections? Explain them.
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer : 

1. Mark phase : In the Mark phase, we mark all the reachable objects. We start from the roots which contains all the local variable and parameters in the functions currently being invoked and any global variables and then trace an object graph. To perform this operation we simply need to do a depth first search approach.


2. Sweep phase: As the name suggests, it "sweeps" the unreachable object. It iterates through all object in the heap. It will remove all object unmarked and clear the marked ones (set unmarked those who are marked).


2. What are the advantages/disadvantages of garbage collection ? 
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer : 

Advantages : 

-	it handles the case of cyclic references so we never ends up in an infinite loop.
-	Automatic memory management 

Disadvantages :

-	The problem of "Fragmentation". We have memory available in "fragments", so we have many free records of small size. It can happened that we want to allocate a record of size bigger than any of the small ones. 
-	Allocation cost.

3. Give another technique that doesn't have this kind of disadvantages
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer :

Copying collection where the idea is to split the heap in two: from/to space.


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Garbage collection: Generational collection by Group 42, Maxime de Streel and Jean-Martin Vlaeminck
===================================================================================================

1. Explain the rationale (or motive) behind generational collection
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:
'''''''

Generational collection relies on the observation that most of the objects die young (e.g., just after one iteration of a loop),
and that objects which are kept for a long time have a high probability of remaining so and kept for an even longer time.

It is therefore beneficial to separate objects into two generations, one for the young objects, and one for the old objects,
and garbage collect more often the young generation than the old one.

2. In général, should we expect a lot of references from objects of the old generation to objects of the young generation? Why?
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:
'''''''

These pointers from old generation to young generation should be rare, because:

- most young objects are only refered from the stack, as they are often only used in one loop of iteration.
- references contained in old objects are often components (in the compositional sense) of these old objects, and so are mostly kept as long as the containing object.


3. Explain the working of the algorithm, that is, how we sweep/collect the two generations, how we determine the generation of an object
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

Answer:
'''''''

The sweep/collect of the young generation is executed more often than for the old generation.

For the young generation, we consider as roots both the usual roots of mark-and-sweep and of Cheney, and all objects present in the old generation.

For the mark and sweep algorithm, we then start from each of these two kinds of roots to mark all objects of the young generation, and then sweep only the objects of the young generation.

For the Cheney algorithm (and other copying collection algorithms), in addition to updating the references to moved objects contained on the stack,
we also have to update references from the old generation objects, which we store in a remembered set.

There are multiple ways of determining if an object becomes old: for instance, we can consider it to become old when it survives two or three collections.
It is important to clearly define the difference, as young objects are garbage-collected more often (because there should be less of them) than old objects.


