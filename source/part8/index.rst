.. _part8:

*************************************************************************************************
Partie 8 | Traits and monads
*************************************************************************************************

Question proposed by Group 15, Florian Duprez and Yorick Tixhon
=====================================================================

Question 1 What are the differences between Traits and Abstrat class? Show a case where you should favorise Traits.
------------


Question 2 What is a monad? What are the 3 monad laws? Give examples
------------


Question 3 A is an array, and B is a List. If you execute this line, what is the output type?:
------------
.. code-block:: scala

	for( i <- B; <- A ) yield (i,j).

Question 4 Rewrite this for loop using only foreach, map, withFilter and flatMap
------------
.. code-block:: scala

	for (i <- list, j <- array , if (i % 2 == 0)){
		yield i*j
	}

Question 5 Implements l.map(f) with flatMap
------------

Question 6 
------------

Consider the following code:

.. code-block:: scala

	trait A {
		def bob(): String = {"A"}
	}
	trait B {
		def bob(): String = {"B"}
	}
	class Test extends A with B
	val test = new Test
	print(test.bob())
	
Will this code give an error message? If not, what is its output?

Consider now the following variation:

.. code-block:: scala

	trait A {
		def bob(): String = {"A"}
	}
	trait AA extends A {
		override def bob(): String = {"AA"}
	}
	trait AB extends A {
		override def bob(): String = {"AB"}
	}
	class Test extends AB with AA
	val test = new Test
	print(test.bob())
	
Will this new code give an error message? And if not, what is its output?